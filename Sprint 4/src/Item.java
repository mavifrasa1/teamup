import javax.swing.JMenuItem;

public class Item implements Comparable<Item>{

	private JMenuItem item;
	private String descrizione="";
	private String ricerca="";
	private String email="";
	private String competenze="";
	
	public Item(String testo){
		item= new JMenuItem(testo);
		
	}
	public Item(String testo, String descrizione, String ricerca){
		this.descrizione = descrizione;
		this.ricerca = ricerca;
		this.email = email;
		this.competenze = competenze;
		item= new JMenuItem(testo);
		
	}
	public Item(String testo, String email, String competenze,String descrizione, String ricerca){
		this.descrizione = descrizione;
		this.ricerca = ricerca;
		this.email = email;
		this.competenze = competenze;
		item= new JMenuItem(testo);
		
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCompetenze() {
		return competenze;
	}
	public void setCompetenze(String competenze) {
		this.competenze = competenze;
	}
	public JMenuItem getItem() {
		return item;
	}

	public void setItem(JMenuItem item) {
		this.item = item;
	}

	@Override
	public int compareTo(Item item) {
		if(!this.item.getText().equals(item.getItem().getText()))
			return 1;
		else
			return 0;
	}
	public String toString() {
		return System.lineSeparator()+item.getText();
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getRicerca() {
		return ricerca;
	}

	public void setRicerca(String ricerca) {
		this.ricerca = ricerca;
	}
	
}
