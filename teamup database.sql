-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Giu 17, 2020 alle 11:03
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teamup`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `dettagli_progetto`
--

CREATE TABLE `dettagli_progetto` (
  `Matricola` char(6) NOT NULL,
  `Id_progetto` char(6) NOT NULL,
  `Data` date NOT NULL,
  `Aggiunto` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `dettagli_progetto`
--

INSERT INTO `dettagli_progetto` (`Matricola`, `Id_progetto`, `Data`, `Aggiunto`) VALUES
('222222', '555555', '1111-11-11', 1),
('222223', '555557', '1999-10-11', 1),
('222222', '555557', '2020-06-16', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `progetti`
--

CREATE TABLE `progetti` (
  `Id` char(6) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Categoria` varchar(20) NOT NULL,
  `Data` date NOT NULL,
  `Ricerca` text DEFAULT NULL,
  `Descrizione` char(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `progetti`
--

INSERT INTO `progetti` (`Id`, `Nome`, `Categoria`, `Data`, `Ricerca`, `Descrizione`) VALUES
('555555', 'Progetto Ingegneria', 'Universita', '2020-04-24', 'Programmatore', 'Dovendo consegnare il progetto d\'ingegneria il prima possibile vorremmo aiuto per il prossimo mese'),
('555556', 'Economia', 'Universita', '2020-04-24', 'Analista', 'progetto economia'),
('555557', 'Calcolo', 'Universita', '2020-04-24', 'Programmatore ', 'progetto semplice python'),
('555558', 'Rip. matematica', 'Scuola', '2020-04-23', 'Maestro', 'Cerco maestro matematica per spiegare calcoli algebrici');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `Matricola` char(6) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `Datanascita` date NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Competenze` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`Matricola`, `Nome`, `Cognome`, `Datanascita`, `Username`, `Password`, `Email`, `Competenze`) VALUES
('222222', 'Sara', 'Scarcelli', '2020-04-24', 'Saretta', '1234', 'sara@libero.it', 'Scrittrice'),
('222223', 'Vincenzo', 'Noia', '1990-02-24', 'Vinc', '1234', 'vinc@hotmail.it', 'Calciatore'),
('222224', 'Marco', 'Macchia', '1997-06-06', 'Marco', '1234', 'marcoooo_97@hotmail.it', 'Insegnante latino'),
('222225', 'Francesco', 'Tritto', '2020-06-04', 'Franc', '1234', 'franc@hotmail.it', 'Insegnante inglese');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `dettagli_progetto`
--
ALTER TABLE `dettagli_progetto`
  ADD KEY `dettagli_progetto_ibfk_1` (`Id_progetto`),
  ADD KEY `dettagli_progetto_ibfk_2` (`Matricola`);

--
-- Indici per le tabelle `progetti`
--
ALTER TABLE `progetti`
  ADD PRIMARY KEY (`Id`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`Matricola`);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `dettagli_progetto`
--
ALTER TABLE `dettagli_progetto`
  ADD CONSTRAINT `dettagli_progetto_ibfk_1` FOREIGN KEY (`Id_progetto`) REFERENCES `progetti` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dettagli_progetto_ibfk_2` FOREIGN KEY (`Matricola`) REFERENCES `utenti` (`Matricola`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
