import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeSet;

public class Database {
	
	private static Connection connessione = null;
	
	public static void collegamentoDatabase() {
		try {
			// caricamento del driver
			new com.mysql.cj.jdbc.Driver();
			/*
			  creazione di una connessione al database Teamup
			  con credenziali di accesso appropriate
			 */
			connessione = DriverManager.getConnection("jdbc:mysql://localhost/teamup","root","");			
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public static void cancellaUtente(Utente utente) {
		String query="DELETE FROM utenti WHERE utenti.Matricola = '" + utente.getMatricola()+"'";
		Statement istruzione;
		ResultSet risultato;
		
		try {
			istruzione = connessione.createStatement();
			istruzione.executeUpdate(query);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static boolean cercaUtente(String user, String pass) {
		
		boolean presente = false;
		String query = "Select * From utenti";
		Statement istruzione;
		try {
			
			istruzione = connessione.createStatement();
			ResultSet risultato = istruzione.executeQuery(query);
	
			while (risultato.next()) {
				if(risultato.getString("Username").equals(user) && risultato.getString("Password").equals(pass)){
					presente = true;
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return presente;
	}


	public static int matricolaRecente() {
		String query = "SELECT Max(Matricola) AS MatricolaAlta FROM utenti";
		Statement istruzione;
		ResultSet risultato;
		int matricola = 0;
		try {
			istruzione = connessione.createStatement();
			risultato = istruzione.executeQuery(query);
			while(risultato.next())
				matricola =(int)risultato.getInt("MatricolaAlta");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return matricola;
		
	}
	
	public static boolean inserisciUtente(Utente utente) {
		String query="INSERT INTO `utenti` (`Matricola`, `Nome`, `Cognome`, `Datanascita`, `Username`, `Password`, `Email`, `Competenze`) VALUES ('"+ utente.getMatricola() +"', '" + utente.getNome() + "', '"+utente.getCognome()+"', '"+utente.getDataNascita()+"', '"+utente.getUsername()+"', '"+utente.getPassword()+"', '"+utente.getEmail()+"', '"+utente.getCompetenze()+"');";
		Statement istruzione;
		boolean risultato = false;
		try {
			istruzione = connessione.createStatement();
			istruzione.executeUpdate(query);
			risultato=true;
			System.out.println(risultato);
		} catch (SQLException e) {
			risultato= false;
			
		}
		return risultato;
	}
	
	public static Utente prendiDatiUtente(String username, String password) {
		String query = "SELECT * FROM utenti WHERE Username='" + username + "' AND Password='" + password + "'";
		Statement istruzione;
		ResultSet risultato;
		Utente utente = new Utente();
		try {
			istruzione = connessione.createStatement();
			risultato = istruzione.executeQuery(query);
			
			while(risultato.next()) {
				utente.setMatricola(risultato.getInt("Matricola")); 
				utente.setNome(risultato.getString("Nome"));
				utente.setCognome(risultato.getString("Cognome"));
				utente.setDataNascita(risultato.getString("DataNascita"));
				utente.setUsername(risultato.getString("Username"));
				utente.setPassword(risultato.getString("Password"));
				utente.setEmail(risultato.getString("Email"));
				utente.setCompetenze(risultato.getString("Competenze"));
			}	
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return utente;
		
	}
	
	
}