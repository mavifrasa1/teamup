import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeSet;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class Gestione {
	private FrameHome home;
	private FrameLogin login;
	private static Utente utente;
	private static Progetto progetto;
	private FrameRegistrazione registrazione;
	private FrameProfiloUtente profiloUtente;
	private FrameModificaProfilo modificaProfilo;
	private static String categoriaScelta="";
	
	public Gestione(FrameHome home, FrameLogin login, FrameRegistrazione registrazione, Utente utente, FrameProfiloUtente profiloUtente, FrameModificaProfilo modificaProfilo) {
		Database.collegamentoDatabase();
		this.login = login;
		this.utente = utente.clone();
		this.registrazione = registrazione;
		this.home = home;
		this.profiloUtente = profiloUtente;
		this.modificaProfilo = modificaProfilo;
		
	}
	

	
	public void controllerLogin() {
		ActionListener gestoreLogin, gestoreReturn;
		gestoreLogin = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				utente =Database.prendiDatiUtente(login.getTxtUser().getText(), login.getTxtPass().getText()).clone();
				System.out.println(utente);
				if(utente.getMatricola()!=0) {
					
					login.rendiVisibile();
					profiloUtente.rendiVisibile();
					profiloUtente.setLblNome(utente.getNome());
					profiloUtente.setLblCognome(utente.getCognome());
					profiloUtente.setLblData(utente.getDataNascita());
					profiloUtente.setLblEmail(utente.getEmail());
					profiloUtente.setLblCompetenze(utente.getCompetenze());
					profiloUtente.setLblMatricola(utente.getMatricola());
					profiloUtente.setLblUsername(utente.getUsername());
					
					
				}
				
			}
		};
		gestoreReturn = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				login.rendiVisibile();
				home.rendiVisibile();
				
			}
		};
		
		login.getBtnLogin().addActionListener(gestoreLogin);
		login.getButtonReturn().addActionListener(gestoreReturn);
	}
	



	
	public void controllerProfiloUtente() {
		ActionListener gestoreCancellaProfiloUtente,gestoreModificaProfiloUtente,gestoreLogout;
		
		
		gestoreLogout = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				home.rendiVisibile();
				profiloUtente.rendiVisibile();
				login.getTxtUser().setText("");
				login.getTxtPass().setText("");
				registrazione.getTxtNome().setText("");
				registrazione.getTxtCognome().setText("");
				registrazione.getTxtData().setText("");
				registrazione.getTxtEmail().setText("");
				registrazione.getTxtCompetenze().setText("");
				registrazione.getTxtUsername().setText("");
				registrazione.getTxtPassword().setText("");
			}
		};
		gestoreCancellaProfiloUtente = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Database.cancellaUtente(utente);
				home.rendiVisibile();
				profiloUtente.rendiVisibile();
			}
		};
		
		
		
		gestoreModificaProfiloUtente = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Utente nuovoUtente;
				profiloUtente.rendiVisibile();
				modificaProfilo.rendiVisibile();
				nuovoUtente = new Utente(utente.getMatricola(), modificaProfilo.getTextNome().getText(), modificaProfilo.getTextCognome().getText(), modificaProfilo.getTextDataNascita().getText(), utente.getUsername(), modificaProfilo.getTextPassword().getText(),  modificaProfilo.getTextEmail().getText(), modificaProfilo.getTextCompetenze().getText() );
				Database.cancellaUtente(utente);
				nuovoUtente = utente.clone();
				
			}
		};
		
		profiloUtente.getBottoneCancellazione().addActionListener(gestoreCancellaProfiloUtente);
		profiloUtente.getBottoneModifica().addActionListener(gestoreModificaProfiloUtente);
		profiloUtente.getBtnLogout().addActionListener(gestoreLogout);
	}
	
	public void controllerHome() {
		ActionListener gestoreHomeLogin, gestoreHomeRegistrati, gestoreHomeCategorie, gestoreReturn;
		
		gestoreReturn = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				home.rendiVisibile();
				
			}
		};
		
		gestoreHomeLogin = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				home.rendiVisibile();
				login.rendiVisibile();
			}
		};
		gestoreHomeRegistrati = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				home.rendiVisibile();
				registrazione.rendiVisibile();
			}
		};
		gestoreHomeCategorie = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				home.rendiVisibile();
			
			}
		};
		
		
		
		home.getBtnLogin().addActionListener(gestoreHomeLogin);
		home.getBtnRegistrati().addActionListener(gestoreHomeRegistrati);
		
	}
	
	public void controllerModificaProfilo() {
		ActionListener gestoreModificaProfilo, gestoreReturn;
		
		gestoreModificaProfilo = new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean esiste=false;
				System.out.println(utente);
				utente=Utente.cloneDaString(utente.getMatricola(),modificaProfilo.getTextNome().getText(),modificaProfilo.getTextCognome().getText(), modificaProfilo.getTextDataNascita().getText(), utente.getUsername(), modificaProfilo.getTextPassword().getText(), modificaProfilo.getTextEmail().getText(), modificaProfilo.getTextCompetenze().getText()).clone();
				

				Database.inserisciUtente(utente);
				
				modificaProfilo.rendiVisibile();
				profiloUtente.rendiVisibile();
				profiloUtente.setLblNome(utente.getNome());
				profiloUtente.setLblCognome(utente.getCognome());
				profiloUtente.setLblData(utente.getDataNascita());
				profiloUtente.setLblEmail(utente.getEmail());
				profiloUtente.setLblCompetenze(utente.getCompetenze());
				profiloUtente.setLblMatricola(utente.getMatricola());
				
			}
		};
		
		gestoreReturn = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {			
				modificaProfilo.rendiVisibile();
				profiloUtente.rendiVisibile();
				
			}
		};
		
		
		modificaProfilo.getButtonReturn().addActionListener(gestoreReturn);
		modificaProfilo.getBottoneConfermaModifica().addActionListener(gestoreModificaProfilo);
	}
	
	public void controllerRegistrazione() {
		ActionListener gestoreRegistrazione, gestoreReturn;
		
		gestoreRegistrazione = new ActionListener() {
		
			

			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean esiste=false;
				utente=new Utente(1+(Database.matricolaRecente()),registrazione.getTxtNome().getText(),registrazione.getTxtCognome().getText(), registrazione.getTxtData().getText(), registrazione.getTxtUsername().getText(), registrazione.getTxtPassword().getText(), registrazione.getTxtEmail().getText(), registrazione.getTxtCompetenze().getText()).clone();
				esiste=Database.inserisciUtente(utente);
				if(esiste==false)
					JOptionPane.showMessageDialog(registrazione.getFrame() , "Dati errati");
				if(esiste==true) {
					registrazione.rendiVisibile();
					profiloUtente.rendiVisibile();
					profiloUtente.setLblNome(utente.getNome());
					profiloUtente.setLblCognome(utente.getCognome());
					profiloUtente.setLblData(utente.getDataNascita());
					profiloUtente.setLblEmail(utente.getEmail());
					profiloUtente.setLblCompetenze(utente.getCompetenze());
					profiloUtente.setLblMatricola(utente.getMatricola());
					profiloUtente.setLblUsername(utente.getUsername());
					profiloUtente.getMenu().removeAll();
					
					
				}
				
			}
		};
		
		gestoreReturn = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				home.rendiVisibile();
				registrazione.rendiVisibile();
				
			}
		};
		
		
		registrazione.getButtonReturn().addActionListener(gestoreReturn);
		registrazione.getBtnRegistrati().addActionListener(gestoreRegistrazione);
	}
	
	
}
