import javax.swing.SwingUtilities;

public class Test{
	
	public static void main(String[] args) {
		
		
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
				FrameHome home = new FrameHome();
				FrameLogin login = new FrameLogin();
				FrameRegistrazione registrazione = new FrameRegistrazione();
				Utente utente = new Utente();
				FrameProfiloUtente frameProfiloUtente = new FrameProfiloUtente();
				FrameModificaProfilo modificaProfilo= new FrameModificaProfilo();
				Gestione gestione= new Gestione( home, login, registrazione, utente, frameProfiloUtente, modificaProfilo);
				gestione.controllerLogin();
				gestione.controllerRegistrazione();
				gestione.controllerHome();
				gestione.controllerProfiloUtente();
				gestione.controllerModificaProfilo();

			}
		});
		
	}
}